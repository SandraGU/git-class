*** Settings ***
Documentation   Existe en un documento de texto los pasos manuales
                Ésta es mi primera automatización
Library         Selenium2Library

*** Variables ***
${palabraABuscar}    casos de prueba
${Navegador}   chrome
${URL}   https://www.google.com

*** Keywords ***
Abrir Navegador y esperar logo
   Open Browser     ${URL}      ${Navegador}
   Wait Until Element Is Visible    xpath=//*[@id="hplogo"]

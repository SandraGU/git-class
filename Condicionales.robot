*** Settings ***
Library     Selenium2Library

*** Variables ***
${Browser}      Chrome
${homepage}     http://automationpractice.com/index.php
${Seleccion}    men

*** Keywords ***
Select Women Option
  Click Element   xpath=//*[@id="block_top_menu"]/ul/li[1]/a
  Title Should Be   Women - My Store

Select Dresses Option
    Click Element   xpath=//*[@id="block_top_menu"]/ul/li[2]/a
    Title Should Browser    Dresses - My Store

*** Test Cases ***
001 Caso con Condicionales
    Open Browser    ${homepage}     ${Browser}
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img
    Run Keyword If      '${Seleccion}'=='women'   Select Women Option   Else    Select Dresses Option
    Close Browser